import findVaccinationDates
from time import sleep
import datetime
import sendEmails
import json

state = ""
city = ""
blocks = []
minimum_age = ""


def loadParameters():
    global state, city, blocks, minimum_age
    location = json.load(open("location.json"))
    state = location["State"]
    city = location["City"]
    blocks = location["Talukas"]
    minimum_age = location["minimum_age"]

    if len(blocks) == 0:
        blocks.append("*")


def runOnce():
    global state, city, blocks, minimum_age

    date = datetime.date.today().strftime("%d-%m-%Y")

    output = findVaccinationDates.findWeeklyAppointments(
        state, city, date, blocks, minimum_age)

    if output is not None:
        sendEmails.sendEmails(output)


def runForever():
    global state, city, blocks, minimum_age

    while True:
        date = datetime.date.today().strftime("%d-%m-%Y")
        output = findVaccinationDates.findWeeklyAppointments(
            state, city, date, blocks, minimum_age)

        if output is not None:
            sendEmails.sendEmails(output)
            print("Mails sent")
            sleep(3600)
        else:
            sleep(120)


if __name__ == '__main__':
    loadParameters()
    # runOnce() # Uncomment this to search and email once
    # runForever() # Uncomment this to search and email continuously
