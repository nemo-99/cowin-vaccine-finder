#!/usr/bin/env python3
import requests
import json
import argparse


def findStateId(stateName: str) -> int:
    response = requests.get(
        "https://cdn-api.co-vin.in/api/v2/admin/location/states")
    states = json.loads(response.text)
    for state in states["states"]:
        if state["state_name"] == stateName:
            return state["state_id"]


def findDistrictId(stateName: str, districtName: str) -> int:
    stateId = findStateId(stateName)
    response = requests.get(
        "https://cdn-api.co-vin.in/api/v2/admin/location/districts/"+str(stateId))
    districts = json.loads(response.text)

    for district in districts["districts"]:
        if district["district_name"] == districtName:
            return district["district_id"]


def findWeeklyAppointments(stateName: str, districtName: str, date: str, talukas: list, minimum_age : int) -> str:
    districtId = findDistrictId(stateName, districtName)
    params = {"district_id": districtId, "date": date}
    response = requests.get(
        "https://cdn-api.co-vin.in/api/v2/appointment/sessions/calendarByDistrict", params=params)

    assert response.status_code == 200, response.content
    centers = json.loads(response.content)
    availableCenters = []
    for center in centers["centers"]:
        if talukas[0] != "*" and center["block_name"] not in talukas:
            continue
        for session in center["sessions"]:
            if session["min_age_limit"] == minimum_age and session["available_capacity"] > 0:
                availableCenters.append(center)
                break

    if len(availableCenters) == 0:
        return None

    output = "{:35} : {:20} : {:10} : {:9} : {:20} : {} \n".format(
        "Name", "Block Name", "Pincode", "Date", "Available Capacity","Fee Type")
    for availableCenter in availableCenters:
        for session in availableCenter["sessions"]:
            output += "{:35} : {:20} : {:10} : {:10}: {:20} : {} \n".format(
                availableCenter["name"], availableCenter["block_name"], availableCenter[
                    "pincode"], session["date"], session["available_capacity"],availableCenter["fee_type"])
    return output

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("stateName",help="Enter the name of your state")
    parser.add_argument("districtName",help="Enter the name of your district")
    parser.add_argument("date",help="Format = dd-MM-yyyy,Appointment information for next 7 days")
    parser.add_argument("age",help="minimum age, enter 18 or 45",type=int)
    parser.add_argument("-b","--blockName",help="Optionally Enter Taluka/Block Name")
    args = parser.parse_args()
    talukas = []

    if args.blockName is None:
        talukas.append("*")
    else:
        talukas.append(args.blockName)

    output = findWeeklyAppointments(args.stateName,args.districtName,args.date,talukas,args.age)
    if output is None:
        print("No slots are available for this week right now. Please try again later")
    else:
        print(output)


if __name__ == "__main__":
    main()