import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import json

def sendEmails(messageBody : str):
    emailInfo = json.load(open("emailInfo.json"))
    senderEmail = emailInfo["sender_email"]
    senderPassword = emailInfo["sender_password"]
    receiverEmails = emailInfo["receiver_emails"]
    message = MIMEMultipart()
    message['From'] = "Vaccine Messenger"
    message['Subject'] = "Cowin Vaccine Update"
    message.attach(MIMEText(messageBody,'plain'))

    with smtplib.SMTP("smtp.gmail.com",587) as session:
        session.starttls()
        session.login(senderEmail,senderPassword)
        text = message.as_string()
        session.sendmail(senderEmail,receiverEmails,text)


